from unittest import TestCase

# method object

class PrimeFactors:
    def __init__(self, number_to_factor):
        self.number_to_factor = number_to_factor
        self.factor = 2

    def value(self):
        factors = []

        while self.can_be_factored():
            while self.is_divisible_by_factor():
                factors.append(self.factor)
                self.next_number_to_factor()
            self.next_factor()

        return factors

    def can_be_factored(self):
        return self.number_to_factor != 1

    def is_divisible_by_factor(self):
        return self.number_to_factor % self.factor == 0

    def next_number_to_factor(self):
        self.number_to_factor = self.number_to_factor / self.factor

    def next_factor(self):
        self.factor = self.factor + 1


class PrimeFactorTestCase(TestCase):
    def test_a(self):
        self.assertEqual(PrimeFactors(1).value(), [])

    def test_b(self):
        self.assertEqual(PrimeFactors(2).value(), [2])

    def test_c(self):
        self.assertEqual(PrimeFactors(3).value(), [3])

    def test_d(self):
        self.assertEqual(PrimeFactors(4).value(), [2, 2])

    def test_e(self):
        self.assertEqual(PrimeFactors(6).value(), [2, 3])

    def test_f(self):
        self.assertEqual(PrimeFactors(8).value(), [2, 2, 2])

    def test_g(self):
        self.assertEqual(PrimeFactors(9).value(), [3, 3])

    def test_h(self):
        factor = 2 * 3 * 5 * 7 * 11 * 13
        self.assertEqual(PrimeFactors(factor).value(), [2, 3, 5, 7, 11, 13])
